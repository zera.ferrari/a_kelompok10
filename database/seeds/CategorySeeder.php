<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'Name'  => 'Politic',
            ],

            [
                'Name'  => 'Lifestyle',
            ],

            [
                'Name'  => 'Sport',
            ],

            [
                'Name'  => 'Relationship',
            ],

            [
                'Name'  => 'Healthy',
            ],
        ]);
    }
}
