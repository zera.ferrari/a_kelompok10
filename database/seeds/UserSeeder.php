<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name'          => 'revolverzera',
                'email'         =>  'revolverzera@command.co', 
                'password'      =>  Hash::make("password"),
                'admin'         =>  1,
            ],
        ]);
    }
}
