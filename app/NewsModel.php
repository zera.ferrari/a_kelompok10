<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsModel extends Model
{
    protected $table = "news";

    protected $primarykey = 'id';

    protected $fillable = [
        'title',
        'description',
        'publisher',
        'id_category',
        'id_comment',
        'comment',
        'avatar',
    ];

    public function category(){
        return $this->hasOne('App\CategoryModel','id_category','id');
    }

    public function comment(){
        return $this->belongsTo('App\CommentModel','id_comment','id');
    }
}
