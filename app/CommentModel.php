<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentModel extends Model
{
    protected $table = "comments";

    protected $primarykey = 'id';

    protected $fillable = [
        'description',
    ];

    public function news(){
        return $this->hasMany('App\NewsModel','id_comment','id');
    }
}
