<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    protected $table = "categories";

    protected $primarykey = 'id';

    protected $fillable = [
        'Name',
    ];

    public function news(){
        return $this->hasOne('App\NewsModel','id_category','id');
    }
}
