<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NewsModel;

class NewsController extends Controller
{
    public function index(){
        $data_news = NewsModel::all();
        return view('News.index', ['data_news'=>$data_news]);
    }

    public function create(){
        return view('News.create');
    }

    public function store(Request $request){
        $request->validate([
            'title'         =>  'required',
            'description'   =>  'required|min:16|max:190',
            'publisher'     =>  'required|min:4|max:16',
            'id_category'   =>  'required|numeric',
            'id_comment'    =>  'required|numeric',
            'comment'       =>  'min:6|max:100',
            'avatar'        =>  'required|image',
        ]);

        if($request->hasFile('avatar')){
            $picture        =   $request->file('avatar');
            $location       =   '/Picture/News/'.time().'.'.$picture->getClientOriginalExtension();
            $directory      =   public_path('/Picture/News');
            $picture->move($directory, $location);
        }

        App\News::create([
            'title'         =>  $request->title,
            'description'   =>  $request->description,
            'publisher'     =>  $request->publisher,
            'id_category'   =>  $request->id_category,
            'id_comment'    =>  $request->id_comment,
            'comment'       =>  $request->comment,
            'avatar'        =>  $location,
        ]);

        return redirect('/News');
    }

    
}
