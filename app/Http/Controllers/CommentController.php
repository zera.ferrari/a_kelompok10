<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class CommentController extends Controller
{

    public function create(){
        return view('Comment.create');
    }

    public function store(Request $request){
        $request->validate([
            'description'   =>  'required|max:191',
        ]);

        App\CommentModel::create([
            'description'   =>  $request->description,
        ]);

        return redirect ('/Comments');
    }

    public function index(){
        $data_comments = App\CommentModel::all();
        return view('Comment.index',['data_comments'=>$data_comments]);
    }

    public function edit($id){
        $edit_comments = App\CommentModel::find($id);
        return view('Comment.edit',['edit_comments'=>$edit_comments]);
    }

    public function update(Request $request, $id){
        $request->validate([
            'description'   =>  'required|max:191',
        ]);

        $edit_comments = App\CommentModel::find($id);
        $edit_comments->description = $request->description;
        $edit_comments->save();

        return redirect ('/Comments');
    }

    public function destroy($id){
        $delete_comments = App\CommentModel::find($id);
        $delete_comments->delete();
        return redirect ('/Comments');
    }
}
