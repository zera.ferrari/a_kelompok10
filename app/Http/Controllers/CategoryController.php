<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    
    public function index()
    {   
        $data_categories = \App\CategoryModel::all();
        return view('Category.index', ['data_categories'=> $data_categories]);
    }

    
    public function create()
    {
        
    }

    
    public function store(Request $request)
    {
        
    }

    
    public function show($id)
    {
        
    }

    
    public function edit($id)
    {
        
    }

    public function update(Request $request, $id)
    {
    
    }

    
    public function destroy($id)
    {
        
    }
}
