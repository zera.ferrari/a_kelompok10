@extends('Admin.app')

@section('title','AdminIndex')
@section('content')
@include('Template.Partials.SideNavigator')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Admin Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in as <strong>ADMIN</strong>
                </div>

		<div class="card-body">
		<div class="container text-primary">
			<form action="{{ route('comments.store')}}" method="POST" class="form-group" enctype="multipart/form-data">
				@csrf

				    <div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="description">Description</label>
					</div>
					<div class="col-md-8">
						<textarea name="description" id="description" cols="50" rows="5">
							{{ ($errors->has('description')) ? $errors->first('description') : "" }}
						</textarea>
					</div>
					</div>
					<br>

					<div class="row">
						<div class="col-md-3 offset-md-5 offset-md-4">
							<button type="submit" class="btn btn-outline-primary">Save</button>
						</div>
					</div>

				</form>
			</div>
		</div>
                
            </div>
        </div>
    </div>
</div>
@endsection
