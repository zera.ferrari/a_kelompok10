@extends('Admin.app')

@section('title','AdminIndex')
@section('content')
@include('Template.Partials.SideNavigator')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Admin Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in as <strong>ADMIN</strong>
                </div>

                
        <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr class="table-primary">
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_categories as $category)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td>{{ $category->Name }}</td>

                            {{-- <td>
                                <a class="btn-sm btn-success d-inline" href="{{route('categories.edit',['id'=>$category['id']]) }}">
                                    <span data-feather="edit-2">Edit</span><span class="sr-only">(current)</span>
                                </a>
                                 --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
