<nav class="col-md-2 d-none d-md-block bg-warning sidebar">
	<div class="sidebar-sticky">
		<ul class="nav flex-column">
			<li class="nav-item ">
				<a class="nav-link active" href="{{ route('categories.index') }}">
					<span data-feather="list"></span>Category<br>
                </a>
				
				<a href="{{ route('comments.index')}} " class="nav-link active">
					<span>Comment</span>
				</a>

				<a href="{{ route('news.index')}} " class="nav-link active">
					<span data-feather="paper">News</span>
				</a>
			</li>
		</ul>
	</div>
</nav>