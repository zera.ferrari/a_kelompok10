@extends('Admin.app')

@section('title','AdminIndex')
@section('content')
@include('Template.Partials.SideNavigator')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Admin Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in as <strong>ADMIN</strong>
                </div>

        <div class="col-md-2">
            <a href="{{route('news.create')}}" class="btn btn-outline-primary">
                <span data-feather="plus-circle"></span>New News <span class="sr-only">(current)</span>
            </a>
        </div>        
        <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr class="table-primary">
                            <th scope="col">ID</th>
                            <th scope="col">Title</th>
                            <th scope="col">Category</th>
                            <th scope="col">Picture</th>
                            <th scope="col">Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_news as $data)
                        <tr>
                            <td>{{ $data->id }}</td>
                            <td>{{ $data->title }}</td>
                            <td>{{ $data->categories->Name}}</td>
                            <td><img src="{{asset($data->avatar)}}" alt="avatar" width="50px" height="50px"></td>

                            

                            <td>
                                <a href="{{route('news.show', ['id'=>$data->id])}}" class="btn-sm btn-primary">
                                    <span data-feather="eye"></span>Detail <span class="sr-only">(current)</span>
                                </a>

                                <a class="btn-sm btn-success d-inline" href="{{route('news.edit',['id'=>$data['id']]) }}">
                                    <span data-feather="edit-2">Edit</span><span class="sr-only">(current)</span>
                                </a>

                                <form action="{{ route('news.destroy', ['id'=>$data['id']]) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn-sm btn-danger" type="submit" value="Delete" name="submit">
                                        <span data-feather="trash"></span>Delete<span class="sr-only">(current)</span>
                                    </button>
                                </form>
                                

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
