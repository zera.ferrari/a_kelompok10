@extends('Admin.app')

@section('title','AdminIndex')
@section('content')
@include('Template.Partials.SideNavigator')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Admin Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in as <strong>ADMIN</strong>
                </div>

		<div class="card-body">
		<div class="container text-primary">
			<form action="{{ route('news.store')}}" method="POST" class="form-group" enctype="multipart/form-data">
				@csrf

				    <div class="row">
					<div class="col-md-3">
						<label class="text-primary" for="title">Title</label>
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="title">
							{{ ($errors->has('title')) ? $errors->first('title') : "" }}
					</div>
					</div>
                    <br>
                    
                    <div class="row">
                        <div class="col-md-3">
                            <label for="description" class="text-primary">Description</label>
                        </div>

                        <div class="col-md-8">
                            <textarea name="" id="" cols="50" rows="5">
                                {{ ($errors->has('description')) ? $errors->first('description') : "" }}
                            </textarea>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-3">
                            <label class="text-primary" for="publisher">Publisher</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="publisher">
                                {{ ($errors->has('publisher')) ? $errors->first('publisher') : "" }}
                        </div>
                        </div>
                        <br>

                        {{-- <div class="row">
                            <div class="col-md-3">
                                <label class="text-primary" for="id_category">Category</label>
                            </div>
                            <div class="col-md-8">
                                <select name="id_category" id="id_category" class="form-control">
                                    @foreach ($data_categories as $data)
                                       <option value="{{$data->id}}">{{$data->Name}}</option> 
                                    @endforeach
                                </select>
                                    {{ ($errors->has('id_category')) ? $errors->first('id_category') : "" }}
                            </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-md-3">
                                    <label class="text-primary" for="id_comment">Comment</label>
                                </div>
                                <div class="col-md-8">
                                    <select name="id_comment" id="id_comment" class="form-control">
                                        @foreach ($data_comments as $data)
                                           <option value="{{$data->id}}">{{$data->description}}</option> 
                                        @endforeach
                                    </select>
                                        {{ ($errors->has('id_comment')) ? $errors->first('id_comment') : "" }}
                                </div>
                                </div>
                                <br> --}}

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="comment" class="text-primary">Comment</label>
                                    </div>
            
                                    <div class="col-md-8">
                                        <textarea name="" id="" cols="50" rows="5">
                                            {{ ($errors->has('comment')) ? $errors->first('comment') : "" }}
                                        </textarea>
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="input-group mb-3">
                                        <div class="col-md-3 text-primary">
                                            Picture
                                        </div>
                                    
                                        <div class="col-md-8">
                                            <div class="custom-file">
                                                <label for="avatar" class="custom-the-label">Upload Picture Here</label>
                                                    <input type="file" name="avatar" id="avatar" class="custom-file-input">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
					<div class="row">
						<div class="col-md-3 offset-md-5 offset-md-4">
							<button type="submit" class="btn btn-outline-primary">Create</button>
						</div>
					</div>

				</form>
			</div>
		</div>
            </div>
        </div>
    </div>
</div>
@endsection
