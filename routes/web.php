<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/entertainment',function(){
    return view('/Template/Homepage');
});

Route::resource('/categories','CategoryController');

Route::group(['prefix'=> '/entertainment', 'middleware'=>['user','auth']], function(){
    Route::resource('/Dashboard/User','UserController');
});
Route::group(['prefix'=> '/entertainment', 'middleware'=>['admin','auth']], function(){
    Route::resource('/Dashboard/Admin','AdminController');
});


Route::get('/Comments','CommentController@index')->name('comments.index');
Route::get('/Comments/Create','CommentController@create')->name('comments.create');
Route::post('Comments','CommentController@store')->name('comments.store');
Route::get('/Comments/{id}/Edit','CommentController@edit')->name('comments.edit');
Route::put('/Comments/{id}','CommentController@update')->name('comments.update');
Route::delete('/Comments/{id}','CommentController@destroy')->name('comments.destroy');


// Route::get('/News','NewsController@index')->name('news.index');
// Route::get('/News/Create','NewsController@create')->name('news.create');
// Route::get('/News','NewsController@store')->name('news.store');

Route::get('/News','NewsController@index')->name('news.index');
Route::get('/News/Create','NewsController@create')->name('news.create');
Route::post('/News','NewsController@store')->name('news.store');